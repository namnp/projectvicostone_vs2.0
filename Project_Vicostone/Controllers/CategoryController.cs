﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicostone.Business.Models;
using Vicostone.Business;
using Vicostone.Business.Implementation;

namespace Project_Vicostone.Controllers
{
    public class CategoryController : BaseController
    {
        private ICategoryService service;

        public CategoryController()
        {
            service=new CategoryService();
        }
        public ActionResult Category(int id)
        {
            var category = service.FindCategory(id);
            ViewBag.ListProducts = service.GetListProducts(id);
            return View(category);
        }
    }
}