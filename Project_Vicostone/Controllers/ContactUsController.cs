﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Common;
using Vicostone.Business;
using Vicostone.Business.Implementation;
using Vicostone.Business.Models;

namespace Project_Vicostone.Controllers
{
    public class ContactUsController : BaseController
    {
        // GET: ContactUs
        IContactUsService service;

        public ContactUsController()
        {
            service = new ContactUsService();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult ContactUs([Bind(Include = "Id,Name,Distributor,Email,Subject,Message")] Contact contacts )
        public ActionResult ContactUs( Contact contacts )
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "6Ld44zQUAAAAACmUAq2jldHMIQiLtBiycmzYwpPp";
            var client = new WebClient();
            var resual = client.DownloadString(string.Format(
                "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(resual);
            var status = (bool)obj.SelectToken("success");


            if (ModelState.IsValid && status)
            {
                var currentCulture = Session[CommonConstants.CurrentCulture];
                contacts.laguage = currentCulture.ToString();
                service.SubmitContact(contacts);
                ViewBag.success = "success";
                return View(contacts);
            }
            else
            {
                ViewBag.error = "error";
                ModelState.AddModelError("", StaticResources.Resources.InsertContactFailed);
            }
            return View(contacts);
        }
    }
}