﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicostone.Business;
using Vicostone.Business.Implementation;
using Vicostone.Business.Models;

namespace Project_Vicostone.Controllers
{
    public class AddGalleryProductController : BaseController
    {
        IAddGalleryProduct service;

        public AddGalleryProductController()
        {
            service=new AddGalleryProductService();
        }

        public ActionResult AddGalleryToProduct()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddGalleryToProduct(Gallery galleryModel, HttpPostedFileBase ImageFileGallery)
        {
            string fileName = Path.GetFileNameWithoutExtension(ImageFileGallery.FileName);
            string extension = Path.GetExtension(ImageFileGallery.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;


            string pathString = @"F:/Project_Vicostone_vs2.1/Project_Vicostone/images/image-Gallery/";
            if (!System.IO.File.Exists(pathString))
                System.IO.Directory.CreateDirectory(pathString);

            galleryModel.ImageGallery = "~/images/image-Gallery/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/images/image-Gallery"), fileName);
            ImageFileGallery.SaveAs(fileName);

            if (ModelState.IsValid)
            {
                try
                {
                    service.AddGallery(galleryModel);
                    return RedirectToAction("AddGalleryToProduct");

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
            }
            return View(galleryModel);
        }
    }
}