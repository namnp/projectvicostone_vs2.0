﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using Vicostone.Business;
using Vicostone.Business.Implementation;
using Vicostone.Business.Models;

namespace Project_Vicostone.Controllers
{
    public class AddProductController : BaseController
    {
        private IAddProductService service;

        public AddProductController()
        {
            service=new AddProductService();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Product imageModel, HttpPostedFileBase ImageFile)
        {
            string fileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
            string extension = Path.GetExtension(ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

            string pathString = @"F:/Project_Vicostone_vs2.1/Project_Vicostone/images/image-Product/";
            if (!System.IO.File.Exists(pathString))
                System.IO.Directory.CreateDirectory(pathString);

            imageModel.ProductImage = "~/images/image-Product/" + fileName;
            fileName = imageModel.ProductImage;
            fileName = Path.Combine(Server.MapPath("~/images/image-Product"), path2: fileName);
            ImageFile.SaveAs(fileName);

            if (ModelState.IsValid)
            {
                service.AddProduct(imageModel);
                return RedirectToAction("Index");
            }
            return View(imageModel);
        }

        
    }
}

