﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Vicostone.Business;
using Vicostone.Business.Implementation;
using Vicostone.Business.Models;

namespace Project_Vicostone.Controllers
{
    public class HomeController : BaseController
    {
        private IHomeService service;

        public HomeController()
        {
            service=new HomeService();
        }

        public ActionResult Index()
        {
            ViewBag.ListProductIndex = service.GetListGalleries();
            return View();
        }

        public ActionResult Product()
        {
            return View(service.GetListProducts());
        }


        //chi tiet san pham 
        public ActionResult ProductDetail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = service.FindProduct(id);
            if (product==null)
            {
                return HttpNotFound();
            }
            ViewBag.RelatedProducts = service.ListRelateProducts(id);
            ViewBag.HueProducts = service.HueProducts(id);
            ViewBag.GalleryProducts = service.GalleriesProducts(id);
            return View(product);
        }

        // hàm search
        public ActionResult Search(string searchString)
        {
            return View(service.SearchProduct(searchString));
        }


        //hien thi san pham theo color ko reload page
        [HttpPost]
        public JsonResult PartialViewProduct(int id, int click)
        {
            var model = ListPartialViewProduct(id, click);
            string viewContent = ConvertViewToString("_PartialViewpProductInHue", model);

            return Json(new { PartialView = viewContent , }, JsonRequestBehavior.AllowGet);
        }


        public List<Product> ListPartialViewProduct(int id, int click)
        {
            if (id == 12)
            {
                return service.GetListProducts();
            }
            return service.GetPartialViewProduct(id, click);
        }

    }
}