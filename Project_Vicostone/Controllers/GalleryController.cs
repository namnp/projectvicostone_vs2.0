﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vicostone.Business;
using Vicostone.Business.Implementation;
using Vicostone.Business.Models;

namespace Project_Vicostone.Controllers
{
    public class GalleryController : BaseController
    {
        private IGalleryService service;

        public GalleryController()
        {
            service= new GalleryService();
        }
        public ActionResult Gallery()
        {
            return View(service.GetAllGallery());
        }

        public ActionResult GalleryToDetail(int id)
        {
            var product = service.FindProduct(id);

            if (product != null)
            {
                return RedirectToAction("ProductDetail", "Home", new {id = product.Id});
            }
            return RedirectToAction("Gallery","Gallery");
        }

    }
}