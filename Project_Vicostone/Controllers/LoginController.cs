﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mail;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Vicostone.Business;
using Vicostone.Business.Implementation;
using Vicostone.Business.Models;
using MailMessage = System.Net.Mail.MailMessage;
using MailPriority = System.Net.Mail.MailPriority;

namespace Project_Vicostone.Controllers
{
    public class LoginController : Controller
    {
        IAccountService service;
        public LoginController()
        {
            service=new AccountService();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Account model)
        {
            if (model.Email != null && model.Password != null)
            {
                if (service.Login(model.Email, Encrypt(model.Password, true)))
                {
                    var email = service.GetByEmail(model.Email);
                    var userSession = new UserLogin();
                    userSession.Email = email.Email;
                    userSession.Password = email.Password;
                    Session.Add(CommonConstants.USER_SESSION, userSession);

                    Session["Name"] = "Hi :" + email.FirstName;

                    return RedirectToAction("index", "Home");
                }
                ModelState.AddModelError("", "Email or Password not validate");
                return View("Login");
            }
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register( Account account)
        {
            if (ModelState.IsValid)
            {
                var acc =  service.FindAccount(account.Email);
                if (acc == null)
                {
                    var p1 = Encrypt(account.Password, true);
                    account.Password = p1;
                    var p2 = Encrypt(account.PasswordConfirm, true);
                    account.PasswordConfirm = p2;
                    service.AddAccount(account);
                    return RedirectToAction("Login");
                }
                else
                {
                    ModelState.AddModelError("", "Account already exists, Please try again");
                }
            }

            return View(account);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["Name"] = null;
            return RedirectToAction("Index", "Home");
        }


        public string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes("hoidapit.com.vn"));
            }
            else keyArray = Encoding.UTF8.GetBytes("hoidapit.com.vn");
            var tdes = new TripleDESCryptoServiceProvider
            {
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public string Pass = "";

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(Account account)
        {
            if (account.Email!=null)
            {
                var acc = service.FindAccount(account.Email);
                if (acc !=null)
                {
                    string content = System.IO.File.ReadAllText(Server.MapPath("~/Views/Login/template/ResetPassword.html"));
                    content = content.Replace("{{Email}}", account.Email);
                    CapNhatMatKhau(account.Email);
                    content = content.Replace("{{Password}}", Pass);
                    var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();
                    new MailHelper().SendMail(account.Email, "Reset Password from website vicostone", content);
                    ViewBag.ToastReset = "please check your email. your passwword was sent";
                    ModelState.AddModelError("", "please check your email. your passwword was sent");
                    return View(account);
                }
                else
                {
                    ModelState.AddModelError("", "Email not registered, Please try again");
                }
            }
            else
            {
                return View();
            }
            return View();
        }

        public string CreateLostPassword(int PasswordLength)
        {
            string _allowedChars = "abcdefghijk0123456789mnopqrstuvwxyz";
            Random randNum = new Random(); char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        private string CapNhatMatKhau(string email)
        {
            var accData = service.FindAccount(email);
            Pass = CreateLostPassword(9);
            accData.Password = Encrypt(Pass, true);
            accData.PasswordConfirm = accData.Password;
            service.SaveChange();
            return accData.Password;
        }
    }
}
