﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StaticResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("StaticResources.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nhà Phân Phối.
        /// </summary>
        public static string Contact_Distributor {
            get {
                return ResourceManager.GetString("Contact_Distributor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Địa chỉ Email.
        /// </summary>
        public static string Contact_Email {
            get {
                return ResourceManager.GetString("Contact_Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chọn.
        /// </summary>
        public static string Contact_Input_Distributor {
            get {
                return ResourceManager.GetString("Contact_Input_Distributor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tin Nhắn.
        /// </summary>
        public static string Contact_Message {
            get {
                return ResourceManager.GetString("Contact_Message", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tên.
        /// </summary>
        public static string Contact_Name {
            get {
                return ResourceManager.GetString("Contact_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Làm ơn chọn.
        /// </summary>
        public static string Contact_RequiredDistributor {
            get {
                return ResourceManager.GetString("Contact_RequiredDistributor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Địa chỉ email không đúng.
        /// </summary>
        public static string Contact_RequiredEmail {
            get {
                return ResourceManager.GetString("Contact_RequiredEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hãy nhập tin nhắn.
        /// </summary>
        public static string Contact_RequiredMessage {
            get {
                return ResourceManager.GetString("Contact_RequiredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tên không đúng.
        /// </summary>
        public static string Contact_RequiredName {
            get {
                return ResourceManager.GetString("Contact_RequiredName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hãy nhập Tiêu đề.
        /// </summary>
        public static string Contact_RequiredSubject {
            get {
                return ResourceManager.GetString("Contact_RequiredSubject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiêu đề.
        /// </summary>
        public static string Contact_Subject {
            get {
                return ResourceManager.GetString("Contact_Subject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gửi.
        /// </summary>
        public static string ContactSubmit {
            get {
                return ResourceManager.GetString("ContactSubmit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiếng Anh.
        /// </summary>
        public static string English {
            get {
                return ResourceManager.GetString("English", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ưu điểm của Vicostone.
        /// </summary>
        public static string Index_Advantages {
            get {
                return ResourceManager.GetString("Index_Advantages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thân thiện với môi trường. An toàn cho người sử dụng..
        /// </summary>
        public static string Index_Envirinmental_better {
            get {
                return ResourceManager.GetString("Index_Envirinmental_better", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cam kết với môi trường.
        /// </summary>
        public static string Index_Environmental_Commitment {
            get {
                return ResourceManager.GetString("Index_Environmental_Commitment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to VICOSTONE cam kết đảm bảo sự hài hòa giữa lợi ích kinh tế và lợi ích môi trường thông qua việc phát triển dòng sản phẩm sinh thái, nghiên cứu và ứng dụng các công nghệ mới, tiết kiệm năng lượng và nâng cao ý thức bảo vệ môi trường cho người lao động.
        ///
        ///Chúng tôi cung cấp đủ nguồn lực nhằm duy trì hiệu quả và cải tiến hệ thống quản lý môi trường theo tiêu chuẩn ISO 14001:2004 và đảm bảo đáp ứng yêu cầu của các chứng chỉ quốc tế như: NSF, Green Guard, Microbial Resistant để khẳng định sản phẩm đá thạch anh V [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Index_Environmental_Commitment_Detail {
            get {
                return ResourceManager.GetString("Index_Environmental_Commitment_Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thư Viện Ảnh.
        /// </summary>
        public static string Index_Inspiration_Gallery {
            get {
                return ResourceManager.GetString("Index_Inspiration_Gallery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tìm Hiểu Thêm.
        /// </summary>
        public static string Index_LearnMore {
            get {
                return ResourceManager.GetString("Index_LearnMore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phân Phối Toàn Cầu.
        /// </summary>
        public static string Index_Our_Global_Presence {
            get {
                return ResourceManager.GetString("Index_Our_Global_Presence", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sản phẩm VICOSTONE được phân phối tại hơn 40 quốc gia khắp 5 châu lục, dưới thương hiệu VICOSTONE và thương hiệu của các nhà phân phối uy tín..
        /// </summary>
        public static string Index_Our_Global_Presence_Detail {
            get {
                return ResourceManager.GetString("Index_Our_Global_Presence_Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to mang thiên nhiên vào nhà bạn.
        /// </summary>
        public static string Index_Title {
            get {
                return ResourceManager.GetString("Index_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Khám phá Bộ sưu tập 2017 của Vicostone.
        /// </summary>
        public static string Index_Title_Detail {
            get {
                return ResourceManager.GetString("Index_Title_Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xem Tất Cả.
        /// </summary>
        public static string Index_View_all {
            get {
                return ResourceManager.GetString("Index_View_all", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Visualizer : tính năng giúp khách hàng phối hợp các sản phẩm với màu sắc, thiết kế khác nhau để tạo nên không gian phù hợp nhất cho căn phòng..
        /// </summary>
        public static string Index_Visualizer_Detail {
            get {
                return ResourceManager.GetString("Index_Visualizer_Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thêm Không Thành Công.
        /// </summary>
        public static string InsertContactFailed {
            get {
                return ResourceManager.GetString("InsertContactFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giới Thiệu.
        /// </summary>
        public static string Layout_about_us {
            get {
                return ResourceManager.GetString("Layout_about_us", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thêm Ảnh Thư Viện.
        /// </summary>
        public static string Layout_Add_Gallery {
            get {
                return ResourceManager.GetString("Layout_Add_Gallery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thêm Sản Phẩm.
        /// </summary>
        public static string Layout_Add_Product {
            get {
                return ResourceManager.GetString("Layout_Add_Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tất cả sản phẩm.
        /// </summary>
        public static string Layout_All_Collection {
            get {
                return ResourceManager.GetString("Layout_All_Collection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Đen.
        /// </summary>
        public static string Layout_Black {
            get {
                return ResourceManager.GetString("Layout_Black", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Nâu.
        /// </summary>
        public static string Layout_Brown {
            get {
                return ResourceManager.GetString("Layout_Brown", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bộ Sưu Tập.
        /// </summary>
        public static string Layout_Collections {
            get {
                return ResourceManager.GetString("Layout_Collections", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sản Phẩm Theo Màu.
        /// </summary>
        public static string Layout_Color {
            get {
                return ResourceManager.GetString("Layout_Color", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Liên Hệ.
        /// </summary>
        public static string Layout_Contact_Us {
            get {
                return ResourceManager.GetString("Layout_Contact_Us", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dịch Vụ Khách Hàng.
        /// </summary>
        public static string Layout_Customer_Service {
            get {
                return ResourceManager.GetString("Layout_Customer_Service", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đối Tác.
        /// </summary>
        public static string Layout_For_the_Trade {
            get {
                return ResourceManager.GetString("Layout_For_the_Trade", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ảnh Ứng Dụng.
        /// </summary>
        public static string Layout_Gallery {
            get {
                return ResourceManager.GetString("Layout_Gallery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Xanh.
        /// </summary>
        public static string Layout_Green {
            get {
                return ResourceManager.GetString("Layout_Green", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Xám.
        /// </summary>
        public static string Layout_Grey {
            get {
                return ResourceManager.GetString("Layout_Grey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quan Hệ Nhà Đầu Tư.
        /// </summary>
        public static string Layout_Investor_Relations {
            get {
                return ResourceManager.GetString("Layout_Investor_Relations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Xám Sáng.
        /// </summary>
        public static string Layout_Light_grey {
            get {
                return ResourceManager.GetString("Layout_Light_grey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đăng Nhập.
        /// </summary>
        public static string Layout_Login {
            get {
                return ResourceManager.GetString("Layout_Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sản Phẩm Mới.
        /// </summary>
        public static string Layout_New_Products {
            get {
                return ResourceManager.GetString("Layout_New_Products", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tin Tức Và Sự Kiện.
        /// </summary>
        public static string Layout_News_Events {
            get {
                return ResourceManager.GetString("Layout_News_Events", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sản Phẩm.
        /// </summary>
        public static string Layout_Product {
            get {
                return ResourceManager.GetString("Layout_Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Đỏ.
        /// </summary>
        public static string Layout_Red {
            get {
                return ResourceManager.GetString("Layout_Red", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Đại Lý.
        /// </summary>
        public static string Layout_Where_to_Buy {
            get {
                return ResourceManager.GetString("Layout_Where_to_Buy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Trắng.
        /// </summary>
        public static string Layout_White {
            get {
                return ResourceManager.GetString("Layout_White", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tất cả sản phẩm.
        /// </summary>
        public static string Product_All_Collection {
            get {
                return ResourceManager.GetString("Product_All_Collection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bảo dưỡng và Bảo trì.
        /// </summary>
        public static string Product_Detail_CARE_MAINTENANCE {
            get {
                return ResourceManager.GetString("Product_Detail_CARE_MAINTENANCE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hướng dẫn sử dụng sản phẩm VICOSTONE
        ///
        ///Nên dùng vải mềm hoặc bọt biển để lau chùi sản phẩm. Có thể sử dụng xà bông, nước sạch hoặc chất tẩy rửa nhẹ không có chất bào mòn để làm sáng bóng bề mặt đá khi vệ sinh hằng ngày.
        ///
        ///Với những vết bẩn cứng đầu:
        ///
        ///-Cà phê hay thực phẩm: dùng mút mềm và nước ấm lau sạch ngay. Nếu cần hãy dùng dung dịch tẩy chuyên dụng như VICOSTONE Cleanser.
        ///
        ///-Sơn móng tay: sử dụng miếng mút có thấm dung dịch tẩy rửa chuyên dụng để lau, sau đó lau lại bằng nước sạch.
        ///
        ///-Bã kẹo cao  [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Product_Detail_CARE_MAINTENANCE_Detail {
            get {
                return ResourceManager.GetString("Product_Detail_CARE_MAINTENANCE_Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sản phẩm liên quan.
        /// </summary>
        public static string Product_Detail_COLLECTION_NAME {
            get {
                return ResourceManager.GetString("Product_Detail_COLLECTION_NAME", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tìm Kiếm Cửa Hàng.
        /// </summary>
        public static string Product_Detail_FIND_A_STORE {
            get {
                return ResourceManager.GetString("Product_Detail_FIND_A_STORE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lorem Ipsum đã trở thành văn bản giả mạo tiêu chuẩn của ngành kể từ những năm 1500, khi một máy in không rõ tên đã lấy một loại bếp loại và chích nó để tạo ra một mẫu sách mẫu. Nó đã sống sót không chỉ năm thế kỷ, mà còn là bước nhảy vọt trong sắp chữ điện tử, về cơ bản vẫn không thay đổi..
        /// </summary>
        public static string Product_detail_Lorem {
            get {
                return ResourceManager.GetString("Product_detail_Lorem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yêu Cầu Mẫu.
        /// </summary>
        public static string Product_Detail_REQUEST_SAMPLE {
            get {
                return ResourceManager.GetString("Product_Detail_REQUEST_SAMPLE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xem Thêm.
        /// </summary>
        public static string Product_Detail_SEE_MORE {
            get {
                return ResourceManager.GetString("Product_Detail_SEE_MORE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chia sẻ lên.
        /// </summary>
        public static string Product_Detail_Share_on {
            get {
                return ResourceManager.GetString("Product_Detail_Share_on", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Màu Tương Tự.
        /// </summary>
        public static string Product_Detail_SIMILAR_HUE {
            get {
                return ResourceManager.GetString("Product_Detail_SIMILAR_HUE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to THÔNG TIN SẢN PHẨM.
        /// </summary>
        public static string Product_detail_TECHNICAL_INFOMATION {
            get {
                return ResourceManager.GetString("Product_detail_TECHNICAL_INFOMATION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kỹ Thuật.
        /// </summary>
        public static string Product_Detail_Technicall {
            get {
                return ResourceManager.GetString("Product_Detail_Technicall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xem toàn tấm.
        /// </summary>
        public static string Product_Detail_VIEW_FULL_SLAB {
            get {
                return ResourceManager.GetString("Product_Detail_VIEW_FULL_SLAB", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Xem Thư Viện.
        /// </summary>
        public static string Product_Detail_VIEWGALLERY {
            get {
                return ResourceManager.GetString("Product_Detail_VIEWGALLERY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nguyên Liệu.
        /// </summary>
        public static string Product_Material {
            get {
                return ResourceManager.GetString("Product_Material", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sản Phẩm Mới.
        /// </summary>
        public static string Product_new_product {
            get {
                return ResourceManager.GetString("Product_new_product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kích Thước.
        /// </summary>
        public static string Product_Size {
            get {
                return ResourceManager.GetString("Product_Size", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lorem Ipsum chỉ đơn giản là văn bản giả của ngành in và sắp chữ. Lorem Ipsum là văn bản giả mạo tiêu chuẩn của ngành kể từ những năm 1500.
        /// </summary>
        public static string Product_Title_Detail {
            get {
                return ResourceManager.GetString("Product_Title_Detail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tiếng Việt.
        /// </summary>
        public static string Vietnamese {
            get {
                return ResourceManager.GetString("Vietnamese", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Liên Hệ Với Chúng Tôi.
        /// </summary>
        public static string ViewBagContactUs {
            get {
                return ResourceManager.GetString("ViewBagContactUs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lorem Ipsum chỉ đơn giản là văn bản giả của ngành in và sắp chữ. Lorem Ipsum đã được tiêu chuẩn của ngành công nghiệp dummy văn bản kể từ những năm 1500,.
        /// </summary>
        public static string ViewBagContactUsTitle {
            get {
                return ResourceManager.GetString("ViewBagContactUsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CÔNG TY CỔ PHẦN VICOSTONE.
        /// </summary>
        public static string ViewBagTitleContact {
            get {
                return ResourceManager.GetString("ViewBagTitleContact", resourceCulture);
            }
        }
    }
}
