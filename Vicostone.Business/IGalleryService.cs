﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business
{
    public interface IGalleryService
    {
        List<Gallery> GetAllGallery();
        Product FindProduct(int id);
    }
}
