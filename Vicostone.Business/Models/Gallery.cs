﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Vicostone.Business.Models
{
    public class Gallery
    {
        [Key]
        public int IdGallery { get; set; }

        [Required]
        public int ProductId { get; set; }

        [StringLength(250)]
        [DisplayName("Updoad File")]
        public string ImageGallery { get; set; }

        public virtual Product Product { get; set; }
    }
}