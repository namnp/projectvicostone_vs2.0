﻿using System.ComponentModel.DataAnnotations;

namespace Vicostone.Business.Models
{
    public class Hues
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(250)]
        public string ImageColor { get; set; }
    }
}