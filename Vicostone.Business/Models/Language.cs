﻿using System.ComponentModel.DataAnnotations;

namespace Vicostone.Business.Models
{
    public class Language
    {
        [Key]
        public string Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public bool IsDefaul { get; set; }
    }
}