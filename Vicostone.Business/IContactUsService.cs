﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business
{
    public interface IContactUsService
    {
        void SubmitContact(Contact contact);
    }
}
