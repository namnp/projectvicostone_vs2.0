﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business
{
    public interface IHomeService
    {
        List<Gallery> GetListGalleries();
        List<Product> GetListProducts();
        Product FindProduct(int? id);
        List<Product> ListRelateProducts(int? id);
        List<Product> HueProducts(int? id);
        List<Gallery> GalleriesProducts(int? id);
        List<Product> SearchProduct(string searchString);
        List<Product> GetPartialViewProduct(int id, int click);
    }
}
