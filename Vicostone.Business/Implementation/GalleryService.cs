﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business.Implementation
{
    public class GalleryService : IGalleryService
    {
        private QuanLyAccountDbContext _db;

        public GalleryService()
        {
            _db = new QuanLyAccountDbContext();
        }

        public List<Gallery> GetAllGallery()
        {
            return _db.Galleries.ToList();
        }

        public Product FindProduct(int id)
        {
            return _db.Products.Find(id);
        }
    }
}
