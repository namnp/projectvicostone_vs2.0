﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business.Implementation
{
    public class AddGalleryProductService:IAddGalleryProduct
    {
        QuanLyAccountDbContext _db;

        public AddGalleryProductService()
        {
            _db=new QuanLyAccountDbContext();
        }


        public void AddGallery(Gallery galleryModel)
        {
            _db.Galleries.Add(galleryModel);
            _db.SaveChanges();
        }
    }
}
