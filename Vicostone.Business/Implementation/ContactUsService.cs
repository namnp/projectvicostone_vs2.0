﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business.Implementation
{
    public class ContactUsService : IContactUsService
    {
        private QuanLyAccountDbContext _db;

        public ContactUsService()
        {
            _db = new QuanLyAccountDbContext();
        }

        public void SubmitContact(Contact contacts)
        {
            _db.Contacts.Add(contacts);
            _db.SaveChanges();
        }

    }
}
