﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business.Implementation
{
    public class HomeService:IHomeService
    {
        QuanLyAccountDbContext _db;

        public HomeService()
        {
            _db=new QuanLyAccountDbContext();
        }

        public List<Gallery> GetListGalleries()
        {
            return _db.Galleries.Take(8).ToList();
        }

        public List<Product> GetListProducts()
        {
            return _db.Products.ToList();
        }

        public Product FindProduct(int? id)
        {
            return  _db.Products.Find(id);
        }

        public List<Product> ListRelateProducts(int? id)
        {
            var product = _db.Products.Find(id);
            return _db.Products.Where(x => x.Id != id && x.Collections == product.Collections).ToList();
        }

        public List<Product> HueProducts(int? id)
        {
            var product = _db.Products.Find(id);
            return _db.Products.Where(x => x.Id != id && x.HueId == product.HueId).ToList();
        }

        public List<Gallery> GalleriesProducts(int? id)
        {
            return _db.Galleries.Where(x => x.ProductId == id).ToList();
        }


        public List<Product> SearchProduct(string searchString)
        {
            var products = from p in _db.Products
                           select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(s => s.ProductName.Contains(searchString) || s.ProductCode.Contains(searchString) || s.Collections.Contains(searchString)
                || s.Sizes.Contains(searchString) || s.TechnicalInfor.Contains(searchString));
            }
            return products.ToList();
        }


        public List<Product> GetPartialViewProduct(int id, int click)
        {
            string categoryName;
            List<Product> product;
            switch (click)
            {
                case 1:
                    categoryName = "New Products";
                    var product1 = _db.Products.Where(x => x.HueId == id && x.Collections == categoryName).ToList();
                    product = product1;
                    break;
                case 2:
                    categoryName = "Classic";
                    var product2 = _db.Products.Where(x => x.HueId == id && x.Collections == categoryName).ToList();
                    product = product2;
                    break;
                case 3:
                    categoryName = "Natural";
                    var product3 = _db.Products.Where(x => x.HueId == id && x.Collections == categoryName).ToList();
                    product = product3;
                    break;
                case 4:
                    categoryName = "Exotic";
                    var product4 = _db.Products.Where(x => x.HueId == id && x.Collections == categoryName).ToList();
                    product = product4;
                    break;
                default:
                    var product5 = _db.Products.Where(x => x.HueId == id).ToList();
                    product = product5;
                    break;
            }
            return product;
        }
    }
}
