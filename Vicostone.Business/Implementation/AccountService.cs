﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business.Implementation
{
    public class AccountService : IAccountService
    {
        private QuanLyAccountDbContext _db;

        public AccountService()
        {
            _db = new QuanLyAccountDbContext();
        }
        public Account GetByEmail(string email)
        {
            return _db.Accounts.SingleOrDefault(x => x.Email == email);
        }

        public bool Login(string email, string password)
        {
            var resual = _db.Accounts.Count(x => x.Email == email && x.Password == password);
            if (resual > 0)
            {
                return true;
            }
            return false;
        }

        public void AddAccount(Account account)
        {
            _db.Accounts.Add(account);
            _db.SaveChanges();
        }

        public Account FindAccount(string email)
        {
            return  _db.Accounts.Find(email);
        }

        public void SaveChange()
        {
            _db.SaveChanges();
        }

     }
}
