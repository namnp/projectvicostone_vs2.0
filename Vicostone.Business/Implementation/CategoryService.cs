﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business.Implementation
{
    public class CategoryService:ICategoryService
    {
        QuanLyAccountDbContext _db;

        public CategoryService()
        {
            _db=new QuanLyAccountDbContext();
        }

        public Category FindCategory(int id)
        {
            return _db.Categories.Find(id);
        }

        public List<Product> GetListProducts(int id)
        {
            if (id == 12)
            {
                return _db.Products.ToList();
            }
            var category = _db.Categories.Find(id);
            return _db.Products.Where(x => x.HueId == id || x.Collections == category.NameCategory).ToList();
        }
    }
}
