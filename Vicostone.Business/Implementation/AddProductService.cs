﻿using System;
using System.IO;
using Vicostone.Business.Models;
using System.Web;

namespace Vicostone.Business.Implementation
{
    public class AddProductService:IAddProductService
    {
        QuanLyAccountDbContext _db;

        public AddProductService()
        {
            _db=new QuanLyAccountDbContext();
        }

        public void AddProduct(Product product)
        {
            _db.Products.Add(product);
            _db.SaveChanges();
        }
    }
}
