﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vicostone.Business.Models;

namespace Vicostone.Business
{
    public interface IAccountService
    {
        Account GetByEmail(string email);
        bool Login(string email, string password);
        void AddAccount(Account account);
        Account FindAccount(string email);
        void SaveChange();
    }
}
