﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Vicostone.Business.Models;

namespace Vicostone.Business
{
    public class QuanLyAccountDbContext:DbContext
    {
        public QuanLyAccountDbContext():base("QuanLyAccountConnectionString")
        {
            
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Hues> Hueses { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Language> Languages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            //ID in Product
            modelBuilder.Entity<Product>().HasKey(a => a.Id);
            modelBuilder.Entity<Product>().Property(a => a.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            base.OnModelCreating(modelBuilder);
            //ID in Gallery
            modelBuilder.Entity<Gallery>().HasKey(a => a.IdGallery);
            modelBuilder.Entity<Gallery>().Property(a => a.IdGallery)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            base.OnModelCreating(modelBuilder);
            //id Contact
            modelBuilder.Entity<Contact>().HasKey(a => a.Id);
            modelBuilder.Entity<Contact>().Property(a => a.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            base.OnModelCreating(modelBuilder);
        }
    }
}


//class config Migrations
//var hues = new List<Models.Hues>
//            {
//                new Hues {Id = 1,ImageColor = "/images/color-hues/white.png"},
//                new Hues {Id = 2,ImageColor = "/images/color-hues/grey.png"},
//                new Hues {Id = 3,ImageColor = "/images/color-hues/light-grey.png"},
//                new Hues {Id = 4,ImageColor = "/images/color-hues/black.png"},
//                new Hues {Id = 5,ImageColor = "/images/color-hues/brown.png"},
//                new Hues {Id = 6,ImageColor = "/images/color-hues/green.png"},
//                new Hues {Id = 7,ImageColor = "/images/color-hues/red.png"},
//            };
//hues.ForEach(s => context.Hueses.Add(s));
//            context.SaveChanges();

//            var category = new List<Models.Category>
//            {
//                new Category {IdCategory = 1,NameCategory = "White"},
//                new Category {IdCategory = 2,NameCategory = "Grey"},
//                new Category {IdCategory = 3,NameCategory = "Light-grey"},
//                new Category {IdCategory = 4,NameCategory = "Black"},
//                new Category {IdCategory = 5,NameCategory = "Brown"},
//                new Category {IdCategory = 6,NameCategory = "Green"},
//                new Category {IdCategory = 7,NameCategory = "Red"},
//                new Category {IdCategory = 8,NameCategory = "New Products"},
//                new Category {IdCategory = 9,NameCategory = "Classic"},
//                new Category {IdCategory = 10,NameCategory = "Natural"},
//                new Category {IdCategory = 11,NameCategory = "Exotic"},
//                new Category {IdCategory = 12,NameCategory = "All Collections"},
//            };
//category.ForEach(s => context.Categories.Add(s));
//            context.SaveChanges();


//            var laguages = new List<Language>
//            {
//                new Language {Id = "vi", Name = "Tiếng Việt", IsDefaul = true},
//                new Language {Id = "en", Name = "Tiếng Anh", IsDefaul = false}
//            };
//laguages.ForEach(s => context.Languages.Add(s));
//            context.SaveChanges();
