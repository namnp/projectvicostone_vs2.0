﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Vicostone.Business.Models;

namespace Vicostone.Business
{
    public interface IAddProductService
    {
        void AddProduct(Product product);
    }
}
